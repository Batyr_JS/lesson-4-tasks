/*
      Задание:
        Создать список дел, в который можно:
        - Добавить новый элемент
        - Удалить элемент
        - Задать что пункт выполнен

      Шаблон элемента:
        <li class="listItem">
            <input type="checkbox" class="DoneCheckbox" />
            <span class="TodoText">{add text}</span>
            <button>Remove</button>
        </li>

    */


window.addEventListener("load", function () {
let myUl=document.getElementById('toDoList');
let myLi=document.getElementsByClassName('listItem');
let newToDoInput=document.getElementById('newToDo');
let newLi;
let newInput;
let newSpan;
let newButnRemove;
let newTask;
let c=0;
let buttons=document.getElementsByTagName('button');
let addButton=document.getElementById('AddToDo');
addButton.addEventListener('click',function(){
    add();
    clearInput();
    
})

function clearInput(){
        newToDoInput.value='';
    }

function add(){
        myUl=document.getElementById('toDoList');
        newLi=document.createElement('li');
        myUl.appendChild(newLi);
        newLi.className='listItem';
        addInput();
        addSpan();
        addButtonRemove();
        c++;
        
    }

function addInput(){
        newInput=document.createElement('input');
        myLi[c].appendChild(newInput);
        newInput.className='DoneCheckbox';
        newInput.type='checkbox'
}

function addSpan(){
    newSpan=document.createElement('span');
    myLi[c].appendChild(newSpan);
    newSpan.className='TodoText';
    newTask=newToDoInput.value;
    newSpan.innerHTML=newTask;
}

function addButtonRemove(){
    newButnRemove=document.createElement('button');
    myLi[c].appendChild(newButnRemove);
    newButnRemove.innerHTML='remove';
    newButnRemove.addEventListener('click',remove);
}


function remove(e){
    c--;
    let parOfBut=e.target.parentElement;
    myUl.removeChild(parOfBut);
    
}
    });