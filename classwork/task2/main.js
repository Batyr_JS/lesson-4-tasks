    /*
      Задание:
        Сделать шиблонизатор элемента по:
        Ширине input type=range
        Цвету  input type=color
        Высоте input type=range
        Скругление углов input type=range
        Padding input type=range

      Свойсвта: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference

        + Сделать выборку цвета из рандомных 20 цветов которые выбираются из появившегося блока :
        https://s3.envato.com/files/230412754/screenshots/1.png
        За заготовку для рандома цвета использовать функцию из первых занятий.

    */

    window.addEventListener("load", function () {
      var myWidth = document.getElementById('myWidth');
        var myheight=document.getElementById('myHeight');
        var myRadius=document.getElementById('myRadius');
        var myPadding=document.getElementById('myPadding');
        var myColor=document.getElementById('myColor');
        var popUp=document.getElementById('popup');
        var bgCol;
        var colorsHolder=document.getElementsByClassName('colorsHolder');
        var colorArr=['red','green','blue','purple','black','yellow','brown','lightgrey','orange','pink','maroon','olive','teal','navy','lime','cyan','magenta','coral','mediumaquamarine','lavender'];
        
        var colorsAddButton=document.getElementById('colorsAddButton');

      myWidth.addEventListener('input', function(event){
        result.style.width = event.target.value + 'px';
      });
        
        myColor.addEventListener('input', function(event){
        result.style.backgroundColor = event.target.value;
      });
        
        myheight.addEventListener('input',function(event){
            result.style.height=event.target.value+'px';
        });
        
         myRadius.addEventListener('input',function(event){
            result.style.borderRadius=event.target.value+'px';
        })
        
         myPadding.addEventListener('input',function(event){
            result.style.padding=event.target.value+'px';
        });
        

        
        colorsAddButton.addEventListener('click',function(){        
            for(let i=0;i<colorsHolder.length;i++){
                    colorsHolder[i].addEventListener('click',function(e){
                    bgCol=e.target.style.backgroundColor;
                    result.style.backgroundColor=bgCol;
                });
                colorsHolder[i].style.backgroundColor=colorArr[i]; 
        }
        });

        
        
        
        
        
        
        
        
    });